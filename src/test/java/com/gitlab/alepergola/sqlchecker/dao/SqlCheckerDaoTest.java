package com.gitlab.alepergola.sqlchecker.dao;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class SqlCheckerDaoTest {

    private final SqlCheckerDao sqlCheckerDao;

    @Autowired
    SqlCheckerDaoTest(JdbcTemplate jdbcTemplate) {
        this.sqlCheckerDao = new SqlCheckerDao(jdbcTemplate);
    }

    @Test
    void executeQuery() {
        var quantities = sqlCheckerDao.executeQuery("SELECT name FROM product WHERE type = 'Sashimi'");
        assertThat(quantities).hasSize(1);
        assertThat(quantities.get(0).keySet()).hasSize(1);
        assertThat(quantities.get(0).get("name")).isEqualTo("Sushi");
    }
}
