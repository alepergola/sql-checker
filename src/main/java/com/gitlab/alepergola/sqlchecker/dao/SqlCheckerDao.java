package com.gitlab.alepergola.sqlchecker.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class SqlCheckerDao {

    private final JdbcTemplate jdbcTemplate;

    public SqlCheckerDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Map<String, Object>> executeQuery(String query) {
        return jdbcTemplate.queryForList(query);
    }

    public String findExerciseQuery(int exercise) {
        return "";
    }

}
