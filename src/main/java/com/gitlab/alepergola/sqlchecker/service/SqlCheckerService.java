package com.gitlab.alepergola.sqlchecker.service;

import com.gitlab.alepergola.sqlchecker.dao.SqlCheckerDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SqlCheckerService {

    private final SqlCheckerDao sqlCheckerDao;

    @Autowired
    public SqlCheckerService(SqlCheckerDao sqlCheckerDao) {
        this.sqlCheckerDao = sqlCheckerDao;
    }

    public String checkQuery(Integer exercise, String userQuery) {
        if (exercise == null) {
            throw new IllegalArgumentException("Exercise must be not null");
        }

        var exerciseResults = getExerciseQueryResult(exercise);
        var userResults = getUserQueryResult(userQuery);

        if (isUserResultCorrect(exerciseResults, userResults)) {
            return "true";
        } else {
            return "false";
        }
    }

    private List<Map<String, Object>> getExerciseQueryResult(Integer exercise) {
        try {
            String exerciseQuery = sqlCheckerDao.findExerciseQuery(exercise);
            return sqlCheckerDao.executeQuery(exerciseQuery);
        } catch (Exception e) {
            throw new RuntimeException("Error retrieving exercise query!");
        }
    }

    private List<Map<String, Object>> getUserQueryResult(String userQuery) {
        try {
            return sqlCheckerDao.executeQuery(userQuery);
        } catch (Exception e) {
            throw new RuntimeException("Error retrieving user query result!");
        }
    }

    private static boolean isUserResultCorrect(List<Map<String, Object>> exerciseResults, List<Map<String, Object>> userResults) {
        //TODO: Implement comparison
        return false;
    }

}
