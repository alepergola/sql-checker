package com.gitlab.alepergola.sqlchecker.controller;

import com.gitlab.alepergola.sqlchecker.service.SqlCheckerService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class SqlCheckerController {

    private final SqlCheckerService service;

    public SqlCheckerController(SqlCheckerService service) {
        this.service = service;
    }

    @RequestMapping(value = "check/{exercise}", method = RequestMethod.POST)
    public String checkQuery(@PathVariable(value = "exercise") Integer exercise, @RequestBody String query) {
        return service.checkQuery(exercise, query);
    }

}
