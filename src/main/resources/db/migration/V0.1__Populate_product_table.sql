Insert into product
  (id, name, type, price, quantity, expire_date)
values
  (1, 'Rossetto', 'Rosso', 50.0, 7, NULL);
Insert into product
  (id, name, type, price, quantity, expire_date)
values
  (2, 'Rossetto', 'Rosa', 50.0, 18, NULL);
Insert into product
  (id, name, type, price, quantity, expire_date)
values
  (3, 'rossetto', 'Nero', 50.0, 10, NULL);
Insert into product
  (id, name, type, price, quantity, expire_date)
values
  (5, 'Motosega', 'Nero', 149.99, 2, NULL);
Insert into product
  (id, name, type, price, quantity, expire_date)
values
  (7, 'Sushi', 'Nighiri', 10.0, 24, SYSDATE + 2);
Insert into product
  (id, name, type, price, quantity, expire_date)
values
  (11, 'Sushi', 'Uramaki', 12.5, 51, SYSDATE);
Insert into product
  (id, name, type, price, quantity, expire_date)
values
  (13, 'Sushi', 'Sashimi', 15.0, 16, SYSDATE + 5);
Insert into product
  (id, name, type, price, quantity, expire_date)
values
  (17, 'Avocado', NULL, 1.29, 4, SYSDATE + 6);
Insert into product
  (id, name, type, price, quantity, expire_date)
values
  (23, 'Patate', NULL, 0.75, 134, SYSDATE + 100);
Insert into product
  (id, name, type, price, quantity, expire_date)
values
  (29, 'Cipolle', NULL, 0.5, 234, SYSDATE + 70);